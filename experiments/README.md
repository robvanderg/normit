In this folder resources can be found to reproduce the results of the paper:

Norm It! Lexical Normalization for Italian and Its Downstream Effects forDependency Parsing

all necessary scripts can be found in the `scripts` folder, see `runAll.sh' for all commands to rerun the experiments and see `genAll.sh' to generate the tables.

