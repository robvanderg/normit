strategies = ['raw', 'pred', 'gold']

for train in strategies:
    outDir = train 
    trainFile = '../../data/train.' + train + '.conllu' 
    cmd = 'python3 src/parser.py --outdir ' + outDir + ' --trainfile ' + trainFile 
    testFile = '../../data/test.' + train + '.conllu' 
    cmd += ' && python3 src/parser.py --modeldir ' + outDir + ' --outdir ' + outDir + ' --testfile ' + testFile + ' --predict > ../../preds/parser.' + train
    print (cmd)
    outDir = 'isdt'
    cmd = 'python3 src/parser.py --modeldir ' + outDir + ' --outdir ' + outDir + ' --testfile ' + testFile + ' --predict > ../../preds/parser.isdt.' + train
    print(cmd)
