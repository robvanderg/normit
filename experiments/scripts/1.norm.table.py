settings = ['noCaps', 'caps', 'capsFeats']

def getData(path):
    data = []
    curSent = []
    for line in open(path):
        if len(line.strip()) < 1:
            data.append(curSent)
            curSent = []
        else:
            curSent.append(line.strip().split())
    return data
print('base, acc, err')

goldData = getData('../annotation_train/train.norm.final')
for setting in settings:
    settingData = getData('preds/it.10fold.' + setting + '.norm')
    all = 0
    cor = 0
    changed = 0
    for goldSent, predSent in zip(goldData, settingData):
        for goldWord, predWord in zip(goldSent, predSent):
            if setting == 'noCaps':
                if goldWord[2].lower() == predWord[0].lower():
                    cor += 1
                if goldWord[2].lower() == goldWord[0].lower():
                    changed += 1
            else:
                if goldWord[2] == predWord[0]:
                    cor += 1
                if goldWord[2] == goldWord[0]:
                    changed += 1
            all += 1
    base = changed/all
    acc = cor/all
    err = (acc-base)/(1-acc)
    print(setting + '    \t{:.2f}\t{:.2f}\t{:.2f}'.format(base*100, acc*100, err*100))

