import Levenshtein


def find(sent, tgt):
    data = []
    for tgtSent in tgt:
        dist = Levenshtein.distance(sent, tgtSent)
        data.append((dist, tgtSent))
    return sorted(data)[0]

def getData(path, wordIdx):
    data = {}
    curSent = []
    sentStr = ''
    for line in open(path, encoding='utf-8', errors='ignore'):
        if len(line) < 2:
            if sentStr in data:
                print('double sentence')
            data[sentStr.strip()] = curSent
            sentStr = ''
            curSent = []
        elif len(line.split('\t')) > 1:
            curSent.append(line.strip().split('\t'))
            if '-' in line.split()[0] and len(line.split()) == 10:
                continue
            sentStr += line.split()[wordIdx] + ' '
    return data


def process(conllData, normData, dest):
    rawFile = open(dest, 'w') 
    goldFile = open(dest.replace('raw', 'gold'), 'w')
    found = set()
    for srcSent in normData:
        dist, tgtSent = find(srcSent, conllData)
        if dist > 10:
            print(dist)
            print(srcSent)
            print(tgtSent)
            print()
        found.add(tgtSent)#TODO for train should print the rest in train
        normSent = normData[srcSent]
        conllSent = conllData[tgtSent]
        for wordIdx in range(len(conllSent)):
            rawFile.write('\t'.join(conllSent[wordIdx]) + '\n')
        rawFile.write('\n')

        srcIdx = 0
        tgtIdx = 0
        normWords = [x[2] for x in normSent]
        conllWords = [x[1] for x in conllSent]
        conllIdx = [x[0] for x in conllSent]
        #for i in range(len(normWords)):
        #    print(i, normWords[i])
        normIdx = 0
        skip = 0
        for wordIdx in range(len(conllWords)):
            #print('\t'.join([str(wordIdx), conllIdx[wordIdx], conllWords[wordIdx], normWords[normIdx], str(normIdx), str(skip)]))
            if skip != 0:
                skip = skip- 1
                pass
            elif '-' not in conllSent[wordIdx][0]:
                conllSent[wordIdx][1] = normSent[normIdx][2]
                normIdx += 1
            else:
                beg = int(conllSent[wordIdx][0].split('-')[0])
                end = int(conllSent[wordIdx][0].split('-')[1])
                skip=end-beg + 1
                normIdx += 1
            goldFile.write('\t'.join(conllSent[wordIdx]) + '\n')
        goldFile.write('\n')
    goldFile.close()
    rawFile.close()
    return

trainConll = getData('UD_Italian-PoSTWITA/it_postwita-ud-train.conllu', 1)
trainConll.update(getData('UD_Italian-PoSTWITA/it_postwita-ud-dev.conllu', 1))
testConll = getData('UD_Italian-PoSTWITA/it_postwita-ud-test.conllu', 1)

trainNorm = getData('../annotation_train/train.norm.final', 0)
testNorm = getData('../annotation_test/test.norm.final', 0)

process(testConll, testNorm, 'data/test.raw.conllu')
process(trainConll, trainNorm, 'data/train.raw.conllu')


