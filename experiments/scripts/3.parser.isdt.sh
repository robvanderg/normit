cd uuparser/barchybrid
git clone https://github.com/UniversalDependencies/UD_Italian-ISDT.git
python3 src/parser.py --outdir isdt --trainfile UD_Italian-ISDT/it_isdt-ud-train.conllu --devfile UD_Italian-ISDT/it_isdt-ud-dev.conllu
python3 src/parser.py --modeldir isdt/ --testfile ../../data/train.raw.conllu --predict --outdir isdt/ > ../../preds/isdt.raw
python3 src/parser.py --modeldir isdt/ --testfile ../../data/train.pred.conllu --predict --outdir isdt/ > ../../preds/isdt.pred
python3 src/parser.py --modeldir isdt/ --testfile ../../data/train.gold.conllu --predict --outdir isdt/ > ../../preds/isdt.gold
cd ../../
