strategies = ['raw', 'pred', 'gold']

def getLAS(train, dev, seed):
    for line in open('preds/' + train + '.' + dev + '.' + str(seed)):
        if line.find('LAS') > 0:
            return float(line.split(' ')[5])
    return 0.0

for train in strategies:
    for dev in strategies:
        total = 0.0
        for seed in range(10):
            total += getLAS(train, dev, seed)
        print(train, dev, '{:.2f}'.format(total/10))

