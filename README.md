# Norm It 

Welcome on the repository for our resources for Italian lexical normalization.

The annotations can be found in the `annotation_test` and `annotation_train` folders. The scripts to obtain the datasplits and calculate the agreements can be found in the `scripts` folder.

To reproduce the experiments reported in our paper, see the `experiments` folder.


