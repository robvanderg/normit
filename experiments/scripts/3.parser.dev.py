strategies = ['raw', 'pred', 'gold']

for train in ['raw', 'pred', 'gold']:
    for i in range(10):
        outDir = train + '.' + str(i)
        trainFile = '../../data/train.' + train + '.conllu.' + str(i) + '.train' 
        devFile = '../../data/train.' + train + '.conllu.' + str(i) + '.dev' 
        cmd = 'python3 src/parser.py --outdir ' + outDir + ' --trainfile ' + trainFile + ' --devfile ' + devFile
        for dev in ['raw', 'pred', 'gold']:
            devFile = '../../data/train.' + dev + '.conllu.' + str(i) + '.dev' 
            cmd += ' && python3 src/parser.py --outdir '  + outDir + ' --modeldir ' + outDir + ' --testfile ' + devFile + " --predict > ../../preds/" + train + '.' + dev + '.' + str(i)
            cmd += ' && mv ' + outDir + '/out.conll ' + train + '.' + dev + '.' + str(i) + '.conllu'
        print(cmd)
           


