from sklearn.metrics import cohen_kappa_score

def containsAlpha(token):
    for char in token:
        if char.isalpha():
            return True
    return False

def fixAnnotation(raw, cor):
    # fix capitalization
    allCaps = 0
    for word in cor:
        if word.upper() == word and containsAlpha(word):
            allCaps+=1
    if allCaps >= 3:
        for i in range(len(cor)):
            cor[i] = cor[i].lower()
        cor[0] = cor[0][0].upper() + cor[0][1:]
    
    # fix clitics
    for i in range(len(cor)):
        if raw[i] in ['l\'', 'mi', 'dell\'']:
            cor[i] = raw[i]
    return cor

data = {}
for i in ['tom', 'alan', 'mich', 'lor']:
    path = 'annotation_test/test.norm.' + str(i)
    raw = []
    cor = []
    for line in open(path):
        #line = line.lower()
        if line[0] == '#':
            continue #TODO
        tok = line.strip().split('\t')
        if len(tok) == 2:
            raw.append(tok[0])
            cor.append(tok[1])
        else:
            # conv to string, because lists are not hashable
            rawSent = '\t'.join(raw)
            if rawSent not in data:
                data[rawSent] = []
            data[rawSent].append(cor)
            raw = []
            cor = []


def same(x, y):
    return y

def getScores(data, transformFunction):
    changedAgree = 0
    changedAgreeSameNorm = 0
    cor = 0
    inCor = 0
    ann1 = []
    ann2 = []
    for raw in data:
        if len(data[raw]) == 2:
            rawTok = raw.split('\t')
            cor1 = transformFunction(rawTok, data[raw][0])
            cor2 = transformFunction(rawTok, data[raw][1])
            for i in range(len(cor1)):
                ann1.append(cor1[i] == rawTok[i])
                ann2.append(cor2[i] == rawTok[i])
                if cor1[i] == cor2[i]:
                    cor += 1
                else:
                    inCor += 1
                if cor1[i] != rawTok[i] and cor2[i] != rawTok[i]:
                    changedAgree += 1
                    if cor1[i] == cor2[i]:
                        changedAgreeSameNorm += 1
    return cohen_kappa_score(ann1, ann2) * 100, changedAgreeSameNorm/changedAgree * 100

befCohen, befAcc = getScores(data, same)
aftCohen, aftAcc = getScores(data, fixAnnotation)

print('    \\begin{tabular}{l l l}')
print('        \\toprule')
print('        Metric           & Before corrections & After corrections\\\\')
print('        \\midrule')
print('        Cohen\'s kappa    & {:.2f}           & {:.2f} \\\\'.format(befCohen, aftCohen))
print('        Word choice acc. & {:.2f}           & {:.2f} \\\\'.format(befAcc, aftAcc))
print('        \\bottomrule')
print('   \\end{tabular}')

