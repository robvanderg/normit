import sys
data = []
curSent = []

for line in open(sys.argv[1]):
    if len(line) > 3:
        curSent.append(line)
    else:
        data.append(curSent)
        curSent = []

for i in range(10):
    beg = int(i * len(data)/10)
    end = int((i+1) * len(data)/10)
    print(beg, end)
    trainFile = open(sys.argv[1] + '.' + str(i) + '.train', 'w')
    devFile = open(sys.argv[1] + '.' + str(i) + '.dev', 'w')
    for i in range(len(data)):
        if i >= beg and i < end:
            for sent in data[i]:
                devFile.write(sent)
            devFile.write('\n')
        else:
            for sent in data[i]:
                trainFile.write(sent)
            trainFile.write('\n')
    trainFile.close()
    devFile.close()

