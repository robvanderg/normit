python3 scripts/1.filter.py UD_Italian-PoSTWITA/it_postwita-ud-train.conllu > tweets.train
python3 scripts/1.filter.py UD_Italian-PoSTWITA/it_postwita-ud-dev.conllu > tweets.dev
python3 scripts/1.filter.py UD_Italian-PoSTWITA/it_postwita-ud-test.conllu > tweets.test

python3 scripts/1.split.py tweets.test 39 39 39 38
python3 scripts/1.split.py tweets.test 25 25 25 25

mv *[0-4] splits/
cat splits/tweets.test.1 splits/tweets.test.3 > annotation/all.1
cat splits/tweets.test.2 splits/tweets.test.1 > annotation/all.2
cat splits/tweets.test.3 splits/tweets.test.2 > annotation/all.3
cat splits/tweets.test.4 splits/tweets.test.4 > annotation/all.4

