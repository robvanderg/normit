import sys

if len(sys.argv) < 3:
    print('please provide input (conllu) file, and minimum number of OOVs')
    exit(1)

def getTrees(path):
    trees = []
    tree = []
    for line in open(path):
        if len(line.strip()) == 0:
            trees.append(tree)
            tree = []
        else:
            tok = line.strip().split('\t')
            tree.append(tok)
    return trees


def containsAlpha(token):
    for char in token:
        if char.isalpha():
            return True
    return False

def notWord(token):
    if 'http' in token:
        return True
    if token[0] == '#' or token[0] == '@':
        return True
    if word == 'token':
        return True
    if word[-1] == '\'':
        return True
    return False

iv = set()
for word in ['mario', 'italia', 'rt', 'roma', 'berlusconi', 'pd', 'perchè', 'pdl', 'tv', 'twitter', 'europa', 'napolitano', 'beppe', 'milano', 'xd', 'of', 'antipolitica', 'lol', 'pietro', 'napoli', 'facebook', ':d', 'silvio', 'presia', 'live', 'tweet', 'news', 'm5s', 'iphone', 'ue', 'nope', ':-d', 'c:', 'vendola']:
    iv.add(word)
for line in open('filtering/aspell.it'):
    iv.add(line.strip().lower())

for path in [sys.argv[1]]:#TODO make smaller
    trees = getTrees(path)
    for tree in trees:
        # merge some words back due to tokenization (add gold annotation here)
        offset = 0
        for i in range(len(tree)):
            if len(tree[i]) != 10:
                pass
            elif '-' in tree[i][0]:
                beg = int(tree[i][0].split('-')[0]) + offset
                end = int(tree[i][0].split('-')[1]) + offset
                norm = [tree[i][1], '']
                for j in range(beg, end + 1):
                    norm[1] += tree[j+2][1] + ' '
                    tree[j+2] = []
                tree[i] = norm
                offset += 1

        ivs = 0
        oovs = 0
        others = 0
        for wordIdx in range(len(tree)):
            if len(tree[wordIdx]) == 10:
                word = tree[wordIdx][1].lower()
                if not containsAlpha(word) or notWord(word):
                    others += 1
                elif tree[wordIdx][1].lower() in iv:
                    ivs += 1
                else:
                    oovs += 1
            elif len(tree[wordIdx]) == 2:
                oovs += 1
        if oovs >= int(sys.argv[2]): #print only noisy tweets
            for item in tree:
                if len(item) == 1:
                    print(item[0])
                elif len(item) == 2:
                    print(item[0] + '\t' + item[1])
                elif len(item) == 10:
                    pass
                    print(item[1] + '\t' + item[1])
            print()
                


