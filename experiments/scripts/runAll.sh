./scripts/0.norm.prep.sh
./scripts/1.norm.train.sh
./scripts/2.runNorm.sh

python3 scripts/3.parser.dev.py > uuparser/barchybrid/dev.sh
cd uuparser/barchybrid
chmod +x dev.sh
./dev.sh
cd ../../

scripts/3.parser.isdt.sh


python3 scripts/3.parser.test.py > uuparser/barchybrid/test.sh
cd uuparser/barchybrid
chmod +x test.sh
./test.sh
cd ../../
#for dev table:
#python3 table.py
#for test table:
#grep LAS preds/parser.*
