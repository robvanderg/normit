python3 scripts/2.prepConl.py

cut -f 2 data/train.raw.conllu > data/train.raw
cut -f 2 data/train.gold.conllu > data/train.gold
paste data/train.raw data/train.raw data/train.gold | sed "s;^               $;;g" > data/train.norm

cut -f 2 data/test.gold.conllu > data/test.gold
cut -f 2 data/test.raw.conllu > data/test.raw
paste data/test.raw data/test.raw | sed "s;^               $;;g" > data/test.norm

exit

cd monoise/src/
./tmp/bin/binary -m KF -i ../../data/train.norm -d ../data/it/ -C  -f 111101111111 -n 16 -W -r ../working/syntactic.10 > ../../data/train.pred 2> ../../data/train.score
./tmp/bin/binary -m TR -i ../../data/train.norm -d ../data/it/ -C  -f 111101111111 -n 16 -r ../working/syntactic
./tmp/bin/binary -m RU -i ../../data/test.norm -d ../data/it -C -f 111101111111 -W -r ../working/syntactic > ../../data/test.pred 2> ../../data/test.score
#alternatively use existing models?
cd ../../


cut -f 1 data/train.gold.conllu > data/train.gold.conllu.1
cut -f 3-99 data/train.gold.conllu > data/train.gold.conllu.3
paste data/train.gold.conllu.1 data/train.pred data/train.gold.conllu.3 | sed "s;^		$;;g" > data/train.pred.conllu

cut -f 1 data/test.gold.conllu > data/test.gold.conllu.1
cut -f 3-99 data/test.gold.conllu > data/test.gold.conllu.3
paste data/test.gold.conllu.1 data/test.pred data/test.gold.conllu.3 | sed "s;^		$;;g" > data/test.pred.conllu

python3 scripts/split.py data/train.raw.conllu
python3 scripts/split.py data/train.gold.conllu
python3 scripts/split.py data/train.pred.conllu

