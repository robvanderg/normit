import random
random.seed(8336)

data = [[]]
for annIdx in range(1,5):
    for lineIdx, line in enumerate(open('annotation_train/train.norm.' + str(annIdx))):
        line = line.replace('\ufeff', '')
        tok = line.strip().split('\t')
        if line[0] == '#':
            data[-1].append(tok)
        elif len(tok) == 0 or (len(tok) == 1 and tok[0] == ''):
            data.append([])
        elif len(tok) == 2:
            tok = [tok[0]] + ['PLACEHOLDER'] + [tok[1]]
            data[-1].append(tok)
        else:
            print('ERROR')
            print(i, lineIdx)
            print(tok, '\n')

#shuffle
random.shuffle(data)

for sentIdx in range(len(data)):
    for wordIdx in range(len(data[sentIdx])):
        print('\t'.join(data[sentIdx][wordIdx]))
    print()

