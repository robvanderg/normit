import sys

if len(sys.argv) < 5:
    print("please provide input file and 4 sizes")
    exit(1)

def getSents(path):
    sents = []
    curSent = []
    for line in open(path):
        if len(line.strip()) > 0:
            curSent.append(line.strip())
        else:
            sents.append(curSent)
            curSent = []
    return sents

sents = getSents(sys.argv[1])

curSent = 0
for i in range(4):
    num = int(sys.argv[2+i])
    outFile = open(sys.argv[1] + '.' + str(i + 1), 'w')
    counter = 0
    while counter != num:
        for line in sents[curSent]:
            outFile.write(line + '\n')
        outFile.write('\n')
        curSent += 1
        counter += 1
    outFile.close()
    
